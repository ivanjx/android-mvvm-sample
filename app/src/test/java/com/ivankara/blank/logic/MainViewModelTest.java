package com.ivankara.blank.logic;

import androidx.databinding.Observable;

import org.junit.Test;

import static org.junit.Assert.*;

public class MainViewModelTest {
    private MainViewModel _vm;

    public MainViewModelTest() {
        _vm = new MainViewModel();
    }

    @Test
    public void counter_test() {
        // Event handler.
        final int[] count = {0};
        _vm.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                ++count[0];
            }
        });

        // Test.
        _vm.setCounter(10);

        // Assert.
        assertEquals(10, _vm.getCounter());
        _vm.setCounter(1);
        assertEquals(2, count[0]);
    }

    @Test
    public void addCounter_test() {
        _vm.addCounter();
        assertEquals(1, _vm.getCounter());
        _vm.addCounter();
        assertEquals(2, _vm.getCounter());
    }
}
