package com.ivankara.blank.logic;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.ivankara.blank.BR;

public class MainViewModel extends BaseObservable {
    private int _counter;

    @Bindable
    public int getCounter() {
        return _counter;
    }

    public void setCounter(int counter) {
        if (counter == _counter) {
            return;
        }

        _counter = counter;
        notifyPropertyChanged(BR.counter);
    }

    public void addCounter() {
        setCounter(getCounter() + 1);
    }
}
