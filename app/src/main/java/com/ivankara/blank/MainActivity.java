package com.ivankara.blank;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.ivankara.blank.databinding.ActivityMainBinding;
import com.ivankara.blank.logic.MainViewModel;

public class MainActivity extends AppCompatActivity {
    private MainViewModel _vm;
    private ActivityMainBinding _binding;

    public MainActivity() {
        _vm = new MainViewModel();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        _binding.setVM(_vm);
    }
}